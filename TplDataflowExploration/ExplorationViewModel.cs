﻿using System.Threading;

namespace TplDataflowExploration
{
	public class ExplorationViewModelEventArgs
	{
	}

	class ExplorationViewModel
	{
		public delegate void VmEventHandler(object sender, ExplorationViewModelEventArgs e);

		private int _running;

		public ExplorationViewModel()
		{
			_running = 0;
		}

		public int SimultaneousWorkerCount => _running;

		public int MaxWorkerThreadCount
		{
			get
			{
				int x;
				int y;
				ThreadPool.GetMaxThreads(out x, out y);
				return x;
			}
		}

		public int WorkerThreadCount
		{
			get
			{
				int x;
				int y;
				ThreadPool.GetAvailableThreads(out x, out y);
				return MaxWorkerThreadCount - x;
			}
		}

		// Declare the event.
		public event VmEventHandler TheEvent;

		public void WorkStarted()
		{
			Interlocked.Increment(ref _running);
			OnTheEvent(new ExplorationViewModelEventArgs());
		}

		public void WorkEnded()
		{
			Interlocked.Decrement(ref _running);
			OnTheEvent(new ExplorationViewModelEventArgs());
		}

		public virtual void OnTheEvent(ExplorationViewModelEventArgs e)
		{
			TheEvent?.Invoke(this, e);
		}
	}
}