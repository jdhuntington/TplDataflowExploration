﻿using System;

namespace TplDataflowExploration
{
	class Program
	{
		static void Main(string[] args)
		{
			var vm = new ExplorationViewModel();
			var scheduler = new ExplorationScheduler(vm);
			var display = new Display(vm);
			display.Present();
			EnqueueWork(scheduler, 10000);
			scheduler.Work().Wait();
			Console.ReadKey();
		}

		private static void EnqueueWork(ExplorationScheduler scheduler, int count)
		{
			var r = new Random();

			for (var i = 0; i < count; i++)
			{
				scheduler.Enqueue(new IdleWorker(TimeSpan.FromMilliseconds(r.Next(500, 10000))));
			}
		}
	}
}