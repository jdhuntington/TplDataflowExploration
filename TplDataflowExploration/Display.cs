﻿using System;

namespace TplDataflowExploration
{
	class Display
	{
		private readonly object _lock = new object();
		private readonly ExplorationViewModel _vm;

		public Display(ExplorationViewModel vm)
		{
			this._vm = vm;
		}

		public void Present()
		{
			Console.SetWindowSize(100, 30);
			Console.SetWindowPosition(0, 0);
			NeatPrint(10, 10, ConsoleColor.Blue, ConsoleColor.Black, "Number of running IdleWorkers:");
			NeatPrint(10, 11, ConsoleColor.Blue, ConsoleColor.Black, "Number of running worker threads:");
			NeatPrint(0, 12, ConsoleColor.Red, ConsoleColor.Yellow, "NB: Not actually number of running threads; a hopeful (but likely wrong) approximation.");
			NeatPrint(10, 15, ConsoleColor.Blue, ConsoleColor.Black, "Max worker thread count:");
			_vm.TheEvent += (sender, args) => { UpdateDisplay(); };
		}

		private void UpdateDisplay()
		{
			lock (_lock)
			{
				NeatPrint(42, 10, ConsoleColor.Red, ConsoleColor.White, _vm.SimultaneousWorkerCount.ToString(), 2, 3);
				NeatPrint(42, 11, ConsoleColor.Yellow, ConsoleColor.Black, _vm.WorkerThreadCount.ToString(), 2, 3);
				NeatPrint(42, 15, ConsoleColor.Green, ConsoleColor.Black, _vm.MaxWorkerThreadCount.ToString(), 2, 3);
			}
		}
		private void NeatPrint(int x, int y, ConsoleColor fg, ConsoleColor bg, string str, int padding = 0, int width = 0)
		{
			if (width == 0)
			{
				width = str.Length;
			}
			var clear = new string(' ', width + (2*padding));
			Console.SetCursorPosition(x - padding, y);
			Console.ForegroundColor = fg;
			Console.BackgroundColor = bg;
			Console.Write(clear);
			Console.SetCursorPosition(x, y);
			Console.Write(str);
			Console.SetCursorPosition(0, 0);
		}
	}
}