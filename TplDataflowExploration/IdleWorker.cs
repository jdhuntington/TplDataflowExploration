﻿using System;
using System.Threading.Tasks;

namespace TplDataflowExploration
{
	class IdleWorker
	{
		public IdleWorker(TimeSpan duration)
		{
			Duration = duration;
		}

		public TimeSpan Duration { get; set; }

		public async Task Work()
		{
			await Task.Delay(Duration);
		}
	}
}