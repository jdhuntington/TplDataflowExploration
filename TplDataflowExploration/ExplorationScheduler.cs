﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace TplDataflowExploration
{
	class ExplorationScheduler
	{
		private ActionBlock<IdleWorker> _actionBlock;
		private BufferBlock<IdleWorker> _bufferBlock;
		private IDisposable _disp;
		private readonly ExplorationViewModel _vm;

		public ExplorationScheduler(ExplorationViewModel vm)
		{
			_vm = vm;
			SetupBlocks();
			PreBufferBlockQueue = new List<IdleWorker>();
		}

		public List<IdleWorker> PreBufferBlockQueue { get; set; }

		public ExecutionDataflowBlockOptions ActionBlockOptions => new ExecutionDataflowBlockOptions
		{
			BoundedCapacity = 400,
			MaxDegreeOfParallelism = 390,
			MaxMessagesPerTask = 1
		};

		private void SetupBlocks()
		{
			_bufferBlock = new BufferBlock<IdleWorker>();
			_actionBlock = new ActionBlock<IdleWorker>(async idleWorker =>
			{
				_vm.WorkStarted();
				await idleWorker.Work();
				_vm.WorkEnded();
			}, ActionBlockOptions);
		}

		public async Task Work()
		{
			_disp = _bufferBlock.LinkTo(_actionBlock, new DataflowLinkOptions {PropagateCompletion = true});
			foreach (var idleWorker in PreBufferBlockQueue)
			{
				await _bufferBlock.SendAsync(idleWorker);
			}
			_bufferBlock.Complete();
			await _actionBlock.Completion;
		}

		public void Enqueue(IdleWorker idleWorker)
		{
			PreBufferBlockQueue.Add(idleWorker);
		}
	}
}