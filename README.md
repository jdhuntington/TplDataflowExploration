# TPL Dataflow Exploration

A quick-n-dirty console program to explore concurrency/parallelism
behavior of the
[TPL Dataflow Library](https://www.nuget.org/packages/Microsoft.Tpl.Dataflow/)
from the .NET TPL.

